package com.example.androidd1hw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Map<String, Product> productInfo = Generator.generate();

        List<String> list = new ArrayList<>(productInfo.keySet());

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        MyAdapter adapter = new MyAdapter(this, list);
        recyclerView.setAdapter(adapter);
        adapter.setListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onClick(String productName) {
                Product.AlertDialog(MainActivity.this, productName, productInfo);
            }
        });
    }
}
