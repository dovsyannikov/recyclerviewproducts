package com.example.androidd1hw;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;

import java.util.Map;

public class Product {
    private String productName;
    private String productDescription;
    private String whereToBuy;
    private float priceInSud;

    public Product(String productName, String productDescription, String whereToBuy, float priceInSud) {
        this.productName = productName;
        this.productDescription = productDescription;
        this.whereToBuy = whereToBuy;
        this.priceInSud = priceInSud;
    }

    public StringBuilder toStringBuilder(){
        StringBuilder result = new StringBuilder();
        result.append("Product Name: " + productName);
        result.append(System.getProperty("line.separator"));
        result.append("Fun Fact: " + productDescription);
        result.append(System.getProperty("line.separator"));
        result.append("Where to buy: " + whereToBuy);
        result.append(System.getProperty("line.separator"));
        result.append("Price in UAH: " + priceInSud);
        return result;
    }

    public static StringBuilder productInfo(String productName, Map<String, Product> productMap){
        return productMap.get(productName).toStringBuilder();
    }

    public static void AlertDialog (Context context, String productName, Map<String, Product> productMap){
        new AlertDialog.Builder(context)
                .setTitle("Product information")
                .setMessage(productInfo(productName, productMap))

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                    }
                })

                .show();
    }
}

