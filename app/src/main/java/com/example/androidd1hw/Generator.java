package com.example.androidd1hw;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Generator {
    public static Map <String, Product> generate(){
        Map <String, Product> products = new HashMap<>();

        products.put("onion", new Product("onion",
                "Onions are cultivated and used around the world",
                "ATB",
                120.4f));
        products.put("strawberry", new Product("strawberry",
                "In 2016, world production of strawberries was 9.2 million tonnes, led by China with 41% of the total.",
                "Market",
                65f));
        products.put("potato", new Product("potato",
                "The importance of the potato as a food source and culinary ingredient varies by region and is still changing",
                "Class",
                12f));
        products.put("chocolate", new Product("chocolate",
                "Chocolate is a usually sweet, brown food preparation of roasted and ground cacao seeds that is made in the form of a liquid",
                "Millenium",
                170f));
        products.put("sweet cherry", new Product("sweet cherry",
                "All parts of the plant except for the ripe fruit are slightly toxic, containing cyanogenic glycosides.",
                "Market",
                56.03f));
        return products;
    }
}
